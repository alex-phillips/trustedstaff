<?php

namespace TNSDash;

class InitShortcodes {

    var $shortcodes;

    function __construct() {

        $this->shortcodes = array(
            'my-profile' => array( $this, 'profile' ),
            'my-preferences' => array( $this, 'preferences' ),
            'my-jobs' => array( $this, 'jobs' ),
        );

        foreach( $this->shortcodes as $name => $callback ) {
            $this->shortcodes = array();
            $this->shortcodes[$name] = new \TNSDASH\Shortcode( $name, $callback );
        }
    }

    function random_password($length)
    {
        //A list of characters that can be used in our
        //random password.
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!-.[]?*()';
        //Create a blank string.
        $password = '';
        //Get the index of the last character in our $characters string.
        $characterListLength = mb_strlen($characters, '8bit') - 1;
        //Loop from 1 to the $length that was specified.
        foreach(range(1, $length) as $i){
            $password .= $characters[random_int(0, $characterListLength)];
        }

        return $password;
    }

    function generate_new_user($id)
    {
        // check if this is to be a new post
        if ($id != 'new_user') {
            return $id;
        }

        $username = strtolower(substr($_POST['acf']['field_5f6e0c7dde0ad']['field_5f4140c341d13'], 0, 1) . $_POST['acf']['field_5f6e0c7dde0ad']['field_5f9a35bf6edcf']);
        $email = $_POST['acf']['field_5f6e0c7dde0ad']['field_5f41333a1f8ec'];
        $counter = 1;
        while (username_exists($username)) {
            $username = "$username$counter";
            $counter++;
        }
        $password = $this->random_password(24);
        $userId = wp_insert_user([
            'user_login' => $username,
            'user_email' => $email,
            'user_pass' => $password,
        ]);

        wp_signon([
            'user_login' => $username,
            'user_password' => $password,
        ]);

        $email_message = <<<__MSG__
Welcome to TrustedStaff! Your temporary password is $password. Please be sure to change it once you log in.
__MSG__;

        wp_mail($email, "Welcome to TrustedStaff!", $email_message);

        return "user_$userId";
    }

    function profile() {
        // Prevent logged-in users from visiting registration page
        if (is_user_logged_in() && preg_match('#^/register/?#', $_SERVER['REQUEST_URI'])) {
            header("Location: /my-profile/");
            exit;
        }

        $welcome_text = '';
        if (isset($_GET['welcome'])) {
            $welcome_text = <<<__HTML__
<h2>Welcome!</h2>
<div>Thank you for registering! Your temporary password has been emailed to you.</div>
__HTML__;

        }

        add_filter('acf/pre_save_post' , [$this, 'generate_new_user'], 10, 1 );

        wp_enqueue_script('dash-scripts');
        wp_enqueue_style('dash-styles');
        ob_start();
        $user = wp_get_current_user();
        echo $welcome_text;
        ?>

<div class="my-profile">



	<div class="edit-fields-form">
        <?php
        acf_form_head();
        acf_form( array(
            'post_id' => $user->ID === 0 ? 'new_user' : 'user_'.$user->ID,
            'field_groups' => array(1487),
            'form' => true,
            'return' => $user->ID === 0 ? "/my-profile/?welcome" : get_permalink(),
            'html_before_fields' => '',
            'html_after_fields' => '',
            'submit_value' => $user->ID === 0 ? 'Register' : 'Update',
            )
        );
        ?>
        <button class="close-edit-form">Cancel</button>
    </div>


</div>

        <?php
        return ob_get_clean();
    }

    function profile_field( $field, $user ) {
        ?>
        <?php if ( !in_array($field, array('address_2', 'address_3')) ): ?>
        	<i class="fas fa-pencil-alt edit-profile-icon"></i>
        <?php endif; ?>
        <?php if (get_field( $field, 'user_'.$user )) : ?>
            <?php echo get_field( $field, 'user_'.$user ); ?>
        <?php else: ?>
            <?php echo $field; ?>
        <?php endif; ?>

        <?php
    }

    function profile_form( $form, $user ) {
        ?>
        <div class="form">
            <?php
            $form_url = '';
            if (get_field( $form, 'user_'.$user )) {
                $form_url = get_field( $form, 'user_'.$user );
            } else {
                $form_url = get_field( $form, 'option' );
            }
            ?>

            <a href="<?php echo $form_url; ?>"><i class="far fa-file-pdf"></i><?php echo $form; ?></a>
        </div>

        <?php
    }

    function profile_upload( $upload, $user ) {
        ?>
        <i class="fas fa-pencil-alt edit-profile-icon"></i>
        <?php if (get_field( $upload, 'user_'.$user )) : ?>
            <?php echo get_field( $upload, 'user_'.$user ); ?>
        <?php endif; ?>

        <?php
    }

    function preferences() {
        wp_enqueue_script('dash-scripts');
        wp_enqueue_style('dash-styles');
        ob_start();
        $user = wp_get_current_user();
        echo '<div class="preferences-form">';
        acf_form_head();
        acf_form( array(
            'post_id' => 'user_'.$user->ID,
            'field_groups' => array(1482),
            'form' => true,
            'return' => get_permalink(),
            'html_before_fields' => '',
            'html_after_fields' => '',
            'submit_value' => 'Update'
            )
        );
        echo '</div>';

        return ob_get_clean();

    }

    function jobs() {
        $locations = get_field('location_of_interest', 'user_'.wp_get_current_user()->ID);
        $specialties = get_field('units_of_interest', 'user_'.wp_get_current_user()->ID);
        $args = array(
            'post_type' => 'job',
            'post_status' => 'publish',
            'posts_per_page' => 2,
            'orderby' => 'date',
            'order' => 'DESC',
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'specialty',
                    'terms' => $specialties,
                ),
                array(
                    'taxonomy' => 'location',
                    'terms' => $locations,
                ),
            ),
        );

        $loop = new \WP_Query( $args );

        ob_start();
        while ( $loop->have_posts() ) : $loop->the_post();
            get_template_part('dashboard/partials/job_summary');
        endwhile;
        ?>

        <a href="/jobs">More Jobs</a>

        <?php
        wp_reset_postdata();

        return ob_get_clean();

    }

}
