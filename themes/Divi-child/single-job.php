<?php
$profession = get_the_terms( get_the_ID(), 'profession' );
$profession_string = join(', ', wp_list_pluck($profession, 'name'));
if(!($profession_string == 'Internal')) {
	if (!is_user_logged_in()) {
		setcookie("ts_redirect", $_SERVER['REQUEST_URI'], time() + 3600, '/');
		wp_redirect( '/your-account' );
		exit;
	}
}
get_header();

$show_default_title = get_post_meta( get_the_ID(), '_et_pb_show_title', true );

wp_enqueue_script('dash-scripts');
wp_enqueue_style('dash-styles');

?>
<div id="job-search-hero" class="single-job">
	<div class="hero-wrapper">
	<h1><?php the_title(); ?></h1>
	</div>
	<?php if (get_field('hot')==='true') : ?>
        <div class="hot_job"><i class="fas fa-fire"></i> HOT JOB</div>
    <?php endif; ?>
</div>
<div id="main-content">

	<div class="container">
		<div id="content-area" class="clearfix">
			<div>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				/**
				 * Fires before the title and post meta on single posts.
				 *
				 * @since 3.18.8
				 */
				do_action( 'et_before_post' );
				?>
				<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>

					<div class="entry-content">

						<?php
						do_action( 'et_before_content' );

						get_template_part('dashboard/partials/job');

						wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->
					<div class="et_post_meta_wrapper">
					<?php
					if ( et_get_option('divi_468_enable') === 'on' ){
						echo '<div class="et-single-post-ad">';
						if ( et_get_option('divi_468_adsense') !== '' ) echo et_core_intentionally_unescaped( et_core_fix_unclosed_html_tags( et_get_option('divi_468_adsense') ), 'html' );
						else { ?>
							<a href="<?php echo esc_url(et_get_option('divi_468_url')); ?>"><img src="<?php echo esc_attr(et_get_option('divi_468_image')); ?>" alt="468" class="foursixeight" /></a>
				<?php 	}
						echo '</div> <!-- .et-single-post-ad -->';
					}

					/**
					 * Fires after the post content on single posts.
					 *
					 * @since 3.18.8
					 */
					do_action( 'et_after_post' );

						if ( ( comments_open() || get_comments_number() ) && 'on' === et_get_option( 'divi_show_postcomments', 'on' ) ) {
							comments_template( '', true );
						}
					?>

					</div> <!-- .et_post_meta_wrapper -->
				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>
			</div> <!-- #left-area -->

		</div> <!-- #content-area -->
	</div> <!-- .container -->
				<div class="search-form-wrapper">
					<div class="search-form">
						<?php echo do_shortcode('[searchandfilter fields="state,profession,specialty" post_type="job" submit_label="Search Jobs"]'); ?>
					</div>
				</div>
</div> <!-- #main-content -->

<?php

get_footer();