(function($) {
  "use strict";

  // Add additional profile links that aren't actually part of the ACF fields
  setTimeout(function () {
    $('.my-profile ul.acf-tab-group').append('<li><a href="#update-preferences-section" class="acf-tab-button" data-placement="top" data-endpoint="0">Preferences</a></li><li><a href="#update-preferences-section" class="acf-tab-button" data-placement="top" data-endpoint="0">Possible Dream Jobs</a></li>')
  }, 1000);

  setTimeout(function () {
    var ui_sections = jQuery('.my-profile ul.acf-tab-group li')
    if (profile.complete.all === true) {
      $('.progress-finished').addClass('completed')
      jQuery('.my-profile ul.acf-tab-group li').each(function(){jQuery(this).addClass('checkmark')})
    } else {
      for (var group in profile.complete) {
        if (profile.complete[group].completed === true) {
          $(ui_sections[profile.complete[group].ui_index]).addClass('checkmark')
        }
      }
    }
  }, 2500);

  var months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ]

  setTimeout(function () {
    $('div.acf-date-picker input.hasDatepicker').each(function () {
      $(this).change(function() {
        var newDate = new Date($(this).val())
        if (isNaN(newDate.getTime())) {
          alert('That date format is invalid')
          return
        }

        $(this).parent().find('input').each(function () {
          $(this).val(months[newDate.getMonth()] + ' ' + newDate.getDate() + ', ' + newDate.getFullYear())
        })
      })
    })
  }, 2500)
}(jQuery));
