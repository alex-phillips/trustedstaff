<?php the_title();
echo "<br>";

$locations = get_the_terms( get_the_ID(), 'location' );
if ($locations) {
    $locations_string = join(', ', wp_list_pluck($locations, 'name'));
    echo 'Location: '.$locations_string."<br>";
}

$specialties = get_the_terms( get_the_ID(), 'specialty' );
if ($specialties) {
    $specialties_string = join(', ', wp_list_pluck($specialties, 'name'));
    echo 'Specialty: '.$specialties_string."<br>";
}

if (get_field('description')) {
    echo get_field('description')."<br>";
}