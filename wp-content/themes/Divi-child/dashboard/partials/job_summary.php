<div class="job-summary">
    <?php if (get_field('hot')==='true') : ?>
        <p class="hot_job"><i class="fas fa-fire"></i> HOT JOB</p>
    <?php endif; ?>
    <div class="img">
        <?php the_post_thumbnail(); ?>
    </div>
    <div class="content">
        <a href="<?php echo get_the_permalink(); ?>"><!--<h3 class="entry-title"><?php the_title(); ?></h3>-->
				<?php 
                    $profession = get_the_terms( get_the_ID(), 'profession' );
					$profession_string = join(', ', wp_list_pluck($profession, 'name'));
					$specialties = get_the_terms( get_the_ID(), 'specialty' );
					$specialties_string = join(', ', wp_list_pluck($specialties, 'name'));
				if( $profession_string == 'Internal' ){ ?>
					<h3 class="profession"><?php the_title(); ?> <span class="specialties"><?php
						if ($specialties) {
							echo $specialties_string;
						} ?>
					</span></h3>
				<?php } else { ?>
					<h3 class="profession"><?php 
						if ($profession) {
							echo $profession_string;
						} ?>
					<span class="specialties"><?php
						if ($specialties) {
							echo $specialties_string;
						} ?>
					</span></h3>
				<?php } ?>
		</a>
        <div class="details">
			<?php $city = get_post_meta(get_the_ID(), 'city', true) ?>
            <p class="location"><i class="fas fa-map-marker-alt"></i><?php echo get_post_meta(get_the_ID(), 'city', true) ?>, <?php echo get_the_terms(get_the_ID(), 'state')[0]->name ?></p>
            <?php if (get_field('regional_area')) : ?>
                <p class="region"><?php echo get_field('regional_area'); ?></p>
            <?php endif; ?>
			 <!--<?php if (is_user_logged_in()) : ?>
                <p class="hospital-name"><i class="far fa-hospital"></i>Hospital Name</p>
            <?php endif; ?>-->
                <!--<p class="profession"><i class="fas fa-medkit"></i><?php 
					$profession = get_the_terms( get_the_ID(), 'profession' );
                if ($profession) {
					$profession_string = join(', ', wp_list_pluck($profession, 'name'));
                    echo $profession_string;
                } ?></p>

            <p class="specialties"><i class="fas fa-stethoscope"></i><?php
				$specialties = get_the_terms( get_the_ID(), 'specialty' );
                if ($specialties) {
                    $specialties_string = join(', ', wp_list_pluck($specialties, 'name'));
                    echo $specialties_string;
                } ?>
            </p>-->

            <?php if (get_field('shift')) : ?>
                <p class="shift"><i class="far fa-clock"></i><?php echo get_field('shift'); ?></p>
			<?php else : ?>
			<p class="shift"><i class="far fa-clock"></i>Shift TBD</p>
            <?php endif; ?>

            <p class="start_date"><i class="far fa-calendar-check"></i><?php if (get_field('start_date')){ 
                $startDate = date_create(get_field('start_date')); 
                     echo date_format($startDate, 'F jS, Y'); 
                    } else {
                    echo "A.S.A.P.";
                     } ?>
            </p>

            <p class="contract-length"><i class="fas fa-hourglass-half"></i><?php 
            if (get_field('duration')) {
            echo get_field('duration');
            } else {
            echo "13 weeks";
            } ?></p>

            <?php if (get_field('job_id')) : ?>
                <div class="job_id"><div class="job-id-label">Job  <span>ID</span></div><?php echo get_field('job_id'); ?></div>
            <?php endif; ?>
			<?php if (is_user_logged_in()) : ?>
                <p class="pay-range"><i class="fas fa-dollar-sign"></i>Pay Range TBD</p>
            <?php endif; ?>
        </div>
		<?php 
		if (is_user_logged_in()) : ?>
        <div class="learn_more_button">
            <a href="<?php echo get_the_permalink(); ?>" title="Learn More">
                <button>Learn More</button>
            </a>
        </div>
		<?php else: ?>
		<?php 
		if( $profession_string == 'Internal' ){ ?>
				<div class="learn_more_button">
					<a href="<?php echo get_the_permalink(); ?>" title="Learn More">
						<button>Learn More</button>
					</a>
				</div>
		<?php } else { ?>
		<div class="learn_more_button">
				<a href="http://trustednursestaffing.force.com/ApplyToJob?jobId=<?php echo get_field('apply_url'); ?>" title="Apply Today">
					<button>Apply Today</button>
				</a>
		</div>
		<?php } ?>
		<!--<div class="log-in-details">
			*Log-In to see Facility, Pay Range and More Details
		</div>-->
		<?php endif; ?>
    </div>
</div>