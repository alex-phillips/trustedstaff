<?php

// Class includes
foreach ( glob( get_stylesheet_directory() . "/dashboard/inc/classes/*.class.php" ) as $file ) {
    include_once $file;
}

// init includes
foreach ( glob( get_stylesheet_directory() . "/dashboard/inc/init/*.php" ) as $file ) {
    include_once $file;
}

new \TNSDash\Init();