<?php

namespace TNSDash;

class UserRole {

    var $userRoleVersion;
    var $role;
    var $display_name;
    var $capabilities;

    function __construct( $role, $display_name, $capabilities, $version = 1 ) {

        $this->role = $role;
        $this->display_name = $display_name;
        $this->capabilities = $capabilities;
        $this->userRoleVersion = $version;

        $this->hooks();

    }

    private function hooks() {
        add_action( 'init', [ $this, 'register_user_role' ] );
    }

    function register_user_role() {
        remove_role($this->role);
        //if ( get_option( 'tns_'.$this->role.'_role_version' ) < $this->userRoleVersion ) {
            add_role(
                $this->role,
                $this->display_name,
                $this->capabilities
            );
            //update_option( 'tns_'.$this->role.'_role_version', $this->userRoleVersion );
        //}
    }

}