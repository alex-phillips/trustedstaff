<?php

namespace TNSDash;

class Feed {

    var $feedname;
    var $display_name;
    var $capabilities;

    function __construct( $feedname ) {

        $this->feedname = $feedname;

        $this->hooks();

    }

    private function hooks() {
        add_action( 'init', [ $this, 'addFeed' ] );
    }

    function addFeed() {
        add_feed( $this->feedname, [ $this, 'feedFunction' ] );
    }

    function feedFunction() {
    	get_template_part( 'dashboard/partials/feed/rss', $this->feedname );
    }

}