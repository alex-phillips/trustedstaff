<?php

namespace TNSDash;

class Shortcode {

    var $name;
    var $callback;

    function __construct( $name, $callback ) {

        $this->name = $name;
        $this->callback = $callback;

        $this->init();

    }

    private function init() {
        add_shortcode( $this->name, $this->callback );
    }

}