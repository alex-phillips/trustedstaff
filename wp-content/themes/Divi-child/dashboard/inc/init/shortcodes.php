<?php

namespace TNSDash;

use ParagonIE\Sodium\Core\Curve25519\Ge\P2;

class InitShortcodes {

    var $shortcodes;

    function __construct() {

        $this->shortcodes = array(
            'my-profile' => array( $this, 'profile' ),
            'my-preferences' => array( $this, 'preferences' ),
            'my-jobs' => array( $this, 'jobs' ),
        );

        foreach( $this->shortcodes as $name => $callback ) {
            $this->shortcodes = array();
            $this->shortcodes[$name] = new \TNSDASH\Shortcode( $name, $callback );
        }
    }

    function random_password($length)
    {
        //A list of characters that can be used in our
        //random password.
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!-.[]?*()';
        //Create a blank string.
        $password = '';
        //Get the index of the last character in our $characters string.
        $characterListLength = mb_strlen($characters, '8bit') - 1;
        //Loop from 1 to the $length that was specified.
        foreach(range(1, $length) as $i){
            $password .= $characters[random_int(0, $characterListLength)];
        }

        return $password;
    }

    /**
     * Propagate the ACF form fields to the actual user data, including creating a new user
     * if the registration took place on the ACF form.
     *
     * @param string $id
     * @return string
     */
    function handle_user_data($id)
    {
        // return if this is not the profile form
        if (!preg_match('#^user_#', $id)) {
            return $id;
        }

        // Pull user-specific information from the ACF form
        $first_name = $_POST['acf']['field_5f6e0c7dde0ad']['field_5f4140c341d13'];
        $last_name = $_POST['acf']['field_5f6e0c7dde0ad']['field_5f9a35bf6edcf'];
        $email = $_POST['acf']['field_5f6e0c7dde0ad']['field_5f41333a1f8ec'];
        $password = $_POST['acf']['field_5f6e0c7dde0ad']['field_5fbc747d1fadf'] ?? false;

        // clear passwords from ACF so it's not stored in plain text
        $_POST['acf']['field_5f6e0c7dde0ad']['field_5fbc747d1fadf'] = '';
        $_POST['acf']['field_5f6e0c7dde0ad']['field_5fbc749b1fae0'] = '';

        if ($id != 'user_new') {
            if (preg_match('#user_\d+#', $id)) {
                $user = wp_get_current_user();
                if ($password) {
                    wp_set_password($password, $user->ID);
                    wp_set_auth_cookie($user->ID);
                    wp_set_current_user($user->ID);
                    do_action('wp_login', $user->user_login, $user);
                }

                wp_update_user([
                    'ID' => $user->ID,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'user_email' => $email,
                ]);
            }

            return $id;
        }

        $username = strtolower(substr($first_name, 0, 1) . $last_name);
        $counter = 1;
        while (username_exists($username)) {
            $username = "$username$counter";
            $counter++;
        }

        $userId = wp_insert_user([
            'user_login' => $username,
            'user_email' => $email,
            'user_pass' => $password,
            'first_name' => $first_name,
            'last_name' => $last_name,
        ]);

        $profile_url = get_option("siteurl") . "/wp-admin/user-edit.php?user_id=$userId&wp_http_referer=%2Fwp-admin%2Fusers.php";
        $msg = <<<__MSG__
A new user as signed up:
<br/><br/>
Email: $email
<br/>
Name: $first_name $last_name
<br/><br/>
View their entire profile and information here: <a href="$profile_url">$profile_url</a>
__MSG__;

        wp_mail(get_option("admin_email"), get_option("blogname") . " - New User Registration", $msg);

        wp_signon([
            'user_login' => $username,
            'user_password' => $password,
        ]);

        return "user_$userId";
    }

    function validate_form($valid, $value, $field, $input_name)
    {
        if ($input_name !== 'acf[field_5f6e0c7dde0ad][field_5fbc747d1fadf]') {
            return $valid;
        }

        $pass1 = $_POST['acf']['field_5f6e0c7dde0ad']['field_5fbc747d1fadf'];
        $pass2 = $_POST['acf']['field_5f6e0c7dde0ad']['field_5fbc749b1fae0'];

        if ($pass1 !== $pass2) {
            return __( 'Passwords must match.' );
        }

        if (is_user_logged_in()) {
            return $valid;
        }

        if (!$pass1 || !$pass2) {
            return __( 'Passwords must match.' );
        }

        return $valid;
    }

    function is_profile_complete()
    {
        $post_id = 'user_'.wp_get_current_user()->ID;
        $groups = [
            'personal_information' => [
                'repeater' => false,
                'fields' => [
                    "first_name" => [
                        "hs_field" => "firstname",
                    ],
                    "last_name" => [
                        "hs_field" => "lastname",
                    ],
                    "email" => [
                        "hs_field" => "email",
                    ],
                    "phone" => [
                        "hs_field" => "phone",
                    ],
                    "address_1" => [
                        "hs_field" => "address",
                    ],
                    // "address_2" => [
                    //     "hs_field" => "",
                    // ],
                    "city" => [
                        "hs_field" => "city",
                    ],
                    "state" => [
                        "hs_field" => "state",
                    ],
                    "zip_code" => [
                        "hs_field" => "zip",
                    ],
                    "date_of_birth" => [
                        "hs_field" => "date_of_birth",
                    ],
                    "work_status" => [
                        "hs_field" => "work_status",
                    ],
                    // "other_work_status" => [
                    //     "hs_field" => "work_status",
                    // ],
                ],
            ],
            'profession' => [
                'repeater' => false,
                'fields' => [
                    "profession-type" => [
                        "hs_field" => "profession_type",
                    ],
                    // "nursing_profession" => [
                    //     "hs_field" => "preferred_profession",
                    // ],
                    // "allied_profession" => [
                    //     "hs_field" => "preferred_profession",
                    // ],
                    // "advanced_practice_profession" => [
                    //     "hs_field" => "preferred_profession",
                    // ],
                    // "non_clinical_practice" => [
                    //     "hs_field" => "preferred_profession",
                    // ],
                    "years_of_experience" => [
                        "hs_field" => "years_of_experience",
                    ],
                    "primary_specialty" => [
                        "hs_field" => "primary_speciality",
                    ],
                    "secondary_specialty" => [
                        "hs_field" => "secondary_specialty",
                    ],
                    "preferred_state_date" => [
                        "hs_field" => "preferred_start_date",
                    ],
                ],
            ],
            'license_information' => [
                'repeater' => true,
                'fields' => [
                    "license_type" => [
                        "hs_field" => "license_information",
                        "type" => "multi_line",
                    ],
                    "license_state" => [
                        "hs_field" => "license_information",
                        "type" => "multi_line",
                    ],
                    "license_number" => [
                        "hs_field" => "license_information",
                        "type" => "multi_line",
                    ],
                    "expiration_date" => [
                        "hs_field" => "license_information",
                        "type" => "multi_line",
                    ],
                    "compact_license" => [
                        "hs_field" => "license_information",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'work_history' => [
                'repeater' => true,
                'nested' => 'work_history',
                'fields' => [
                    "facility" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "type" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "position" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    // "teaching_facility" => [
                    //     "hs_field" => "work_history",
                    //     "type" => "multi_line",
                    // ],
                    "location" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "state_date" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    // "end_date" => [
                    //     "hs_field" => "work_history",
                    //     "type" => "multi_line",
                    // ],
                    // "current_position" => [
                    //     "hs_field" => "work_history",
                    //     "type" => "multi_line",
                    // ],
                    "specialty" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "shift" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "charge_experience" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "nurse_to_patient_ratio" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "beds_in_unit" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "#_beds_in_facility" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "reason_for_leaving" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "reference_name" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "reference_phone" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                    "ok_to_contact" => [
                        "hs_field" => "work_history",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'education' => [
                'repeater' => true,
                'nested' => 'degrees',
                'fields' => [
                    "degree" => [
                        "hs_field" => "education_information",
                        "type" => "multi_line",
                    ],
                    "school" => [
                        "hs_field" => "education_information",
                        "type" => "multi_line",
                    ],
                    "location" => [
                        "hs_field" => "education_information",
                        "type" => "multi_line",
                    ],
                    "started" => [
                        "hs_field" => "education_information",
                        "type" => "multi_line",
                    ],
                    "graduated" => [
                        "hs_field" => "education_information",
                        "type" => "multi_line",
                    ],
                    // "currently_attending" => [
                    //     "hs_field" => "education_information",
                    //     "type" => "multi_line",
                    // ],
                ],
            ],
            'references' => [
                'repeater' => true,
                'fields' => [
                    "facilityhospital" => [
                        "hs_field" => "references",
                        "type" => "multi_line",
                    ],
                    "name" => [
                        "hs_field" => "references",
                        "type" => "multi_line",
                    ],
                    "title" => [
                        "hs_field" => "references",
                        "type" => "multi_line",
                    ],
                    "email" => [
                        "hs_field" => "references",
                        "type" => "multi_line",
                    ],
                    "phone" => [
                        "hs_field" => "references",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'credentials' => [
                'repeater' => true,
                'fields' => [
                    "certification" => [
                        "hs_field" => "credentials",
                        "type" => "multi_line",
                    ],
                    // "other_certification" => [
                    //     "hs_field" => "credentials",
                    //     "type" => "multi_line",
                    // ],
                    "issue_date" => [
                        "hs_field" => "credentials",
                        "type" => "multi_line",
                    ],
                    "expiration_date" => [
                        "hs_field" => "credentials",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'fit_test' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'fit_test',
                'fields' => [
                    "test_date" => [
                        "hs_field" => "fit_test",
                        "type" => "multi_line",
                    ],
                    "expiration_date" => [
                        "hs_field" => "fit_test",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'flu_shot' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'flu_shot',
                'fields' => [
                    "test_date" => [
                        "hs_field" => "flu_shot",
                        "type" => "multi_line",
                    ],
                    "expiration_date" => [
                        "hs_field" => "flu_shot",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'hep_b' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'hep_b',
                'fields' => [
                    "date_1" => [
                        "hs_field" => "hep_b_vaccines",
                        "type" => "multi_line",
                    ],
                    "date_2" => [
                        "hs_field" => "hep_b_vaccines",
                        "type" => "multi_line",
                    ],
                    "date_3" => [
                        "hs_field" => "hep_b_vaccines",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'heb_b_titre' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'hep_b_titre',
                'fields' => [
                    "titre_date" => [
                        "hs_field" => "heb_b_titre",
                        "type" => "multi_line",
                    ],
                    "result_-_positive" => [
                        "hs_field" => "heb_b_titre",
                        "type" => "multi_line",
                    ],
                    "result_-_negative" => [
                        "hs_field" => "heb_b_titre",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'mmr' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'mmr',
                'fields' => [
                    "date_1" => [
                        "hs_field" => "mmr_vaccines",
                        "type" => "multi_line",
                    ],
                    "date_2" => [
                        "hs_field" => "mmr_vaccines",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'mmr_titre' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'mmr_titre',
                'fields' => [
                    "titre_date" => [
                        "hs_field" => "mmr_titre",
                        "type" => "multi_line",
                    ],
                    "result_-_positive" => [
                        "hs_field" => "mmr_titre",
                        "type" => "multi_line",
                    ],
                    "result_-_negative" => [
                        "hs_field" => "mmr_titre",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'physical' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'physical',
                'fields' => [
                    "test_date" => [
                        "hs_field" => "physical",
                        "type" => "multi_line",
                    ],
                    "expiration_date" => [
                        "hs_field" => "physical",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'ppd' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'ppd',
                'fields' => [
                    "place_date" => [
                        "hs_field" => "ppd",
                        "type" => "multi_line",
                    ],
                    "read_date" => [
                        "hs_field" => "ppd",
                        "type" => "multi_line",
                    ],
                    "expiration_date" => [
                        "hs_field" => "ppd",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'ppd_2' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'ppd_2',
                'fields' => [
                    "place_date" => [
                        "hs_field" => "n2nd_step_ppd",
                        "type" => "multi_line",
                    ],
                    "read_date" => [
                        "hs_field" => "n2nd_step_ppd",
                        "type" => "multi_line",
                    ],
                    "expiration_date" => [
                        "hs_field" => "n2nd_step_ppd",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'igra_test' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'igra_test',
                'fields' => [
                    "test_date" => [
                        "hs_field" => "igra_test",
                        "type" => "multi_line",
                    ],
                    "expiration_date" => [
                        "hs_field" => "igra_test",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'tdap' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'igra_test',
                'fields' => [
                    "date" => [
                        "hs_field" => "tdap",
                        "type" => "multi_line",
                    ],
                    "expiration_date" => [
                        "hs_field" => "tdap",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'varicella' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'varicella',
                'fields' => [
                    "date_1" => [
                        "hs_field" => "varicella_vaccines",
                        "type" => "multi_line",
                    ],
                    "date_2" => [
                        "hs_field" => "varicella_vaccines",
                        "type" => "multi_line",
                    ],
                ],
            ],
            'varicella_titre' => [
                'repeater' => false,
                'group_field_name' => 'medical_history',
                'nested' => 'varicella_titre',
                'fields' => [
                    "titre_date" => [
                        "hs_field" => "varicella_titre",
                        "type" => "multi_line",
                    ],
                    "result_-_positive" => [
                        "hs_field" => "varicella_titre",
                        "type" => "multi_line",
                    ],
                    "result_-_negative" => [
                        "hs_field" => "varicella_titre",
                        "type" => "multi_line",
                    ],
                ],
            ],
        ];

        $groups_completed = [
            'all' => true,
        ];
        $ui_checkmark_counter = 0;
        foreach ($groups as $group => &$group_config) {
            $group = $group_config['group_field_name'] ?? $group;

            if (!isset($groups_completed[$group])) {
                $groups_completed[$group] = [
                    'completed' => true,
                    'ui_index' => $ui_checkmark_counter,
                ];
            }

            // Handle grouped data
            $group = $group_config['group_field_name'] ?? $group;
            $grouped_fields = get_field($group, $post_id);
            if (isset(($group_config['nested']))) {
                $grouped_fields = $grouped_fields[$group_config['nested']];
            }

            if ($group_config['repeater'] === false) {
                $grouped_fields = [$grouped_fields];
            }

            $fields = $group_config['fields'];

            foreach ($grouped_fields as $group_fields) {
                foreach ($group_fields as $key => $value) {
                    $config = $fields[$key] ?? null;
                    if (!$config) {
                        continue;
                    }

                    if (!$value) {
                        $groups_completed['all'] = false;
                        $groups_completed[$group]['completed'] = false;
                    }
                }
            }

            $ui_checkmark_counter++;
        }

        return $groups_completed;
    }

    function profile() {
        // Prevent logged-in users from visiting registration page
        if (is_user_logged_in() && preg_match('#^/register/?#', $_SERVER['REQUEST_URI'])) {
            header("Location: /my-profile/");
            exit;
        }

        $welcome_text = '';
        if (isset($_GET['welcome'])) {
            $welcome_text = <<<__HTML__
<h2>Welcome!</h2>
<div>Thank you for registering!</div>
__HTML__;

        }

        add_filter('acf/pre_save_post' , [$this, 'handle_user_data'], 10, 1 );
        add_filter('acf/validate_value', [$this, 'validate_form'], 10, 4);

        wp_enqueue_script('dash-scripts');
        wp_enqueue_style('dash-styles');

        wp_enqueue_script('my-profile-js', get_stylesheet_directory_uri() . "/js/my-profile.js");
        wp_localize_script('my-profile-js', 'profile', [
            'complete' => $this->is_profile_complete(),
        ]);

        ob_start();
        $user = wp_get_current_user();

        echo $welcome_text;
        ?>

<div class="my-profile">



	<div class="edit-fields-form">
        <?php
        acf_form_head();
        acf_form( array(
            'post_id' => $user->ID === 0 ? 'user_new' : 'user_'.$user->ID,
            'field_groups' => array(1487),
            'form' => true,
            'return' => $user->ID === 0 ? "/my-profile/?welcome" : get_permalink(),
            'html_before_fields' => '',
            'html_after_fields' => '',
            'submit_value' => $user->ID === 0 ? 'Register' : 'Update',
            )
        );
        ?>
        <button class="close-edit-form">Cancel</button>
    </div>


</div>

        <?php
        return ob_get_clean();
    }

    function profile_field( $field, $user ) {
        ?>
        <?php if ( !in_array($field, array('address_2', 'address_3')) ): ?>
        	<i class="fas fa-pencil-alt edit-profile-icon"></i>
        <?php endif; ?>
        <?php if (get_field( $field, 'user_'.$user )) : ?>
            <?php echo get_field( $field, 'user_'.$user ); ?>
        <?php else: ?>
            <?php echo $field; ?>
        <?php endif; ?>

        <?php
    }

    function profile_form( $form, $user ) {
        ?>
        <div class="form">
            <?php
            $form_url = '';
            if (get_field( $form, 'user_'.$user )) {
                $form_url = get_field( $form, 'user_'.$user );
            } else {
                $form_url = get_field( $form, 'option' );
            }
            ?>

            <a href="<?php echo $form_url; ?>"><i class="far fa-file-pdf"></i><?php echo $form; ?></a>
        </div>

        <?php
    }

    function profile_upload( $upload, $user ) {
        ?>
        <i class="fas fa-pencil-alt edit-profile-icon"></i>
        <?php if (get_field( $upload, 'user_'.$user )) : ?>
            <?php echo get_field( $upload, 'user_'.$user ); ?>
        <?php endif; ?>

        <?php
    }

    function preferences() {
        wp_enqueue_script('dash-scripts');
        wp_enqueue_style('dash-styles');
        ob_start();
        $user = wp_get_current_user();
        echo '<div class="preferences-form">';
        acf_form_head();
        acf_form( array(
            'post_id' => 'user_'.$user->ID,
            'field_groups' => array(1482),
            'form' => true,
            'return' => get_permalink(),
            'html_before_fields' => '',
            'html_after_fields' => '',
            'submit_value' => 'Update'
            )
        );
        echo '</div>';

        return ob_get_clean();

    }

    function jobs() {
        $locations = get_field('location_of_interest', 'user_'.wp_get_current_user()->ID);
        $specialties = get_field('units_of_interest', 'user_'.wp_get_current_user()->ID);

        if (!$specialties || !$locations) {
            $args = array(
                'post_type' => 'job',
                'post_status' => 'publish',
                'posts_per_page' => 2,
                'orderby' => 'date',
                'order' => 'DESC',
                'tax_query' => array(
                    'relation' => 'OR',
                    array(
                        'taxonomy' => 'specialty',
                        'terms' => $specialties,
                    ),
                    array(
                        'taxonomy' => 'state',
                        'terms' => $locations,
                    ),
                ),
            );
        } else {
            $args = array(
                'post_type' => 'job',
                'post_status' => 'publish',
                'posts_per_page' => 2,
                'orderby' => 'date',
                'order' => 'DESC',
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'specialty',
                        'terms' => $specialties,
                    ),
                    array(
                        'taxonomy' => 'state',
                        'terms' => $locations,
                    ),
                ),
            );
        }

        $loop = new \WP_Query( $args );

        ob_start();
        while ( $loop->have_posts() ) : $loop->the_post();
            get_template_part('dashboard/partials/job_summary');
        endwhile;
        ?>

        <a href="/jobs">More Jobs</a>

        <?php
        wp_reset_postdata();

        return ob_get_clean();

    }

}
