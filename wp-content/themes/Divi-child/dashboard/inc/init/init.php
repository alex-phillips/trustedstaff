<?php

namespace TNSDash;

class Init {

    var $userRoleInitiator;

    function __construct() {
        $userRoleInitiator = new \TNSDash\InitUserRoles();
        $shortcodeInitiator = new \TNSDash\InitShortcodes();
        $feedInitiator = new \TNSDash\InitFeeds();

        $this->hooks();
    }

    function hooks() {
        add_action( 'wp_enqueue_scripts', array($this, 'register_scripts_styles' ) );

        add_action( 'init', array( $this, 'job_post_type' ) );
        add_action( 'init', array( $this, 'job_taxonomy' ) );

        acf_add_options_page(array(
            'page_title' 	=> 'Dashboard Settings',
            'menu_title'	=> 'Dashboard Settings',
            'menu_slug' 	=> 'dashboard-settings',
            'capability'	=> 'edit_posts',
            'redirect'		=> false
        ));

        add_filter( 'wp_nav_menu_items', array( $this, 'login_menu_item'), 10, 2 );

        add_action( 'user_register', array( $this, 'new_user') );

    }

    function register_scripts_styles() {
        wp_register_style( 'dash-styles', get_stylesheet_directory_uri().'/dashboard/dash.css' );
        wp_register_script( 'dash-scripts', get_stylesheet_directory_uri().'/dashboard/dash.js' );
    }

    function login_menu_item( $items, $args ) {
        if (is_user_logged_in() && $args->menu_id == 'et-secondary-nav') {
            $items = '<li class="log-out-btn"><a href="'. wp_logout_url() .'">Log Out</a></li>' . $items;
            $items = '<li class="my-profile-btn"><a href="'. get_permalink( '68' ) .'">My Profile<i class="fas fa-user"></i></a></li>' . $items;
        }
        elseif (!is_user_logged_in() && $args->menu_id == 'et-secondary-nav') {
            $items = '<li class="log-in-btn"><a href="' . get_permalink( '67' ) . '">Log In <i class="fas fa-sign-in-alt"></i></a></li>' . $items;
        }
        return $items;
    }

    function job_post_type() {
        register_post_type( 'job',
            array(
                'labels' => array(
                    'name' => __( 'Jobs' ),
                    'singular_name' => __( 'Job' )
                ),
                'public' => true,
                'has_archive' => true,
                'rewrite' => array('slug' => 'jobs'),
                'show_in_rest' => true,
                'supports' => array('title', 'custom-fields', 'thumbnail')
            )
        );
    }

    function job_taxonomy() {
        register_taxonomy(
            'state',
            'job',
            array(
                'labels' => array(
                    'name'              => _x( 'Job State', 'taxonomy general name', 'dashboard' ),
                    'singular_name'     => _x( 'Job State', 'taxonomy singular name', 'dashboard' ),
                    'search_items'      => __( 'Search State', 'dashboard' ),
                    'all_items'         => __( 'All State', 'dashboard' ),
                    'edit_item'         => __( 'Edit State', 'dashboard' ),
                    'update_item'       => __( 'Update State', 'dashboard' ),
                    'add_new_item'      => __( 'Add New State', 'dashboard' ),
                    'new_item_name'     => __( 'New State Name', 'dashboard' ),
                    'menu_name'         => __( 'State', 'dashboard' ),
                )
            )
        );
        register_taxonomy(
            'profession',
            'job',
            array(
                'labels' => array(
                    'name'              => _x( 'Professions', 'taxonomy general name', 'dashboard' ),
                    'singular_name'     => _x( 'Profession', 'taxonomy singular name', 'dashboard' ),
                    'search_items'      => __( 'Search Professionss', 'dashboard' ),
                    'all_items'         => __( 'All Professions', 'dashboard' ),
                    'edit_item'         => __( 'Edit Profession', 'dashboard' ),
                    'update_item'       => __( 'Update Profession', 'dashboard' ),
                    'add_new_item'      => __( 'Add New Profession', 'dashboard' ),
                    'new_item_name'     => __( 'New Profession Name', 'dashboard' ),
                    'menu_name'         => __( 'Professions', 'dashboard' ),
                ),
                'rewrite' => array(
                  'slug' => 'professions', // This controls the base slug that will display before each term
                ),
            )
        );
        register_taxonomy(
            'specialty',
            'job',
            array(
                'labels' => array(
                    'name'              => _x( 'Specialties', 'taxonomy general name', 'dashboard' ),
                    'singular_name'     => _x( 'Specialty', 'taxonomy singular name', 'dashboard' ),
                    'search_items'      => __( 'Search Specialties', 'dashboard' ),
                    'all_items'         => __( 'All Specialties', 'dashboard' ),
                    'edit_item'         => __( 'Edit Specialty', 'dashboard' ),
                    'update_item'       => __( 'Update Specialty', 'dashboard' ),
                    'add_new_item'      => __( 'Add New Specialty', 'dashboard' ),
                    'new_item_name'     => __( 'New Specialty Name', 'dashboard' ),
                    'menu_name'         => __( 'Specialties', 'dashboard' ),
                ),
                'rewrite' => array(
                  'slug' => 'specialties', // This controls the base slug that will display before each term
                ),
            )
        );
        register_taxonomy(
            'job_tag',
            'job',
            array( )
        );
    }

    function new_user( $user_id ) {
        $user = get_user_by( 'id', $user_id );
        update_field('name', $user->display_name, 'user_'.$user_id);
        update_field('email', $user->user_email, 'user_'.$user_id);
    }

}
