<?php

namespace TNSDash;

class InitFeeds {

    var $feedsConfig;
    var $feeds;

    function __construct() {

        $this->feedsConfig = array(
            'jobs'
        );

        $this->feeds = array();
        foreach( $this->feedsConfig as $feed ) {
            $this->feeds[$feed] = new \TNSDASH\Feed( $feed );
        }
    }

}