<?php

namespace TNSDash;

class InitUserRoles {

    var $userRoleConfig;
    var $userRoles;

    function __construct() {

        $this->userRoleConfig = array(
            'employee' => array(
                'roleName' => 'Employee',
                'capabilities' => array(
                    'read' => true,
                    'manage_employee_self' => true,
                ),
                'version' => 1.3,
            ),
            
        );

        foreach( $this->userRoleConfig as $role => $config ) {
            $this->userRoles = array();
            $this->userRoles[$role] = new \TNSDASH\UserRole( $role, $config['roleName'], $config['capabilities'], $config['version'] );
        }
    }

}