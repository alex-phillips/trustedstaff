jQuery(document).ready(function($) {

    $('.edit-profile-icon').click(function() {
        $('.edit-fields-form').css('display', 'block');
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".edit-fields-form").offset().top - 200
        }, 500);
    });
    $('.close-edit-form').click(function() {
        $('.edit-fields-form').css('display', 'none');
        $([document.documentElement, document.body]).animate({
            scrollTop: 0
        }, 500);
    });
    
    $('.skills-checklist-button').click(function() {
        $('.skills-checklist').css('top', $('#page-container').css('padding-top'));
        $('.skills-checklist').addClass('active');
        
    });
    $('.close-skills-checklist').click(function() {
        $('.skills-checklist').removeClass('active');
    });

    $('.resume-button').click(function() {
        $('.edit-fields-form').css('display', 'block');
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".edit-fields-form .acf-field-5f414d3dc9967").offset().top - 200
        }, 1000);
    });

    $('.certifications-button').click(function() {
        $('.edit-fields-form').css('display', 'block');
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".edit-fields-form .acf-field-5f414d4bc9968").offset().top - 200
        }, 1000);
    });

    $('.my-profile .profile-columns .employment-forms .timesheets').click(function() {
        $('.my-profile .profile-columns .employment-forms .timesheet').toggleClass('active');
    });

    $('button.search-text').click(function() {
        $('.search-container').toggleClass('active');
    });

})