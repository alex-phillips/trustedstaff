<?php

 get_header();

wp_enqueue_script('dash-scripts');
wp_enqueue_style('dash-styles');
 ?>
<div id="job-search-hero">
	<h1><span class="pre-title">Travel Nursing:</span><br><?php single_term_title(); ?></h1>
</div>
<div id="main-content">
	<section class="state-intro updated">
	<div class="et_pb_row">
		<?php $taxonomyTerm = get_queried_object(); ?>
		<?php if (get_field('intro_text', $taxonomyTerm)) : ?>
			<div class="intro-text"><?php echo get_field('intro_text', $taxonomyTerm); ?></div>
        <?php endif; ?>
	</div>
	</section>

	<section class="create-account-button">
		<a href="/your-account/"><button>Create a Free Profile</button></a>
		<div>To See Exclusive Travel Nursing Jobs in <?php single_term_title(); ?></div>
	</section>
	<div class="container search">
		<div id="content-area" class="clearfix">
			<h2>Find Current Travel Nurse Jobs in <?php single_term_title(); ?></h2>
			<div class="search-form">
					<?php echo do_shortcode('[searchandfilter fields="state,profession,specialty" post_type="job" submit_label="Search Jobs"]'); ?>
				</div>
			
			<h2>Browse Some Active Travel Nurse Jobs in <?php single_term_title(); ?></h2>
			<div id="job-area">
				<div class="job-icon-key">
					<!--<?php if (is_user_logged_in()) : ?>
					<div class="job-icon">
						<i class="far fa-hospital"></i> Facility
					</div>
					<?php endif; ?>
					<div class="job-icon">
						<i class="fas fa-medkit"></i> Profession
					</div>
					<div class="job-icon">
						<i class="fas fa-stethoscope"></i> Specialties
					</div>-->
					<div class="job-icon">
						<i class="fas fa-map-marker-alt"></i> Location
					</div>
					<div class="job-icon">
						<i class="far fa-calendar-check"></i> Start Date
					</div>
					<div class="job-icon">
						<i class="far fa-clock"></i> Shift
					</div>
					<div class="job-icon">
						<i class="fas fa-hourglass-half"></i> Contract Length
					</div>
					<?php if (is_user_logged_in()) : ?>
					<div class="job-icon">
						<i class="fas fa-dollar-sign"></i> Pay Range
					</div>	
					<?php endif; ?>
				</div>
		<?php if ( have_posts() ) : ?>
			<ul class="grid-search">
				<?php while ( have_posts() ) : the_post();
					$post_format = et_pb_post_format(); ?>
					<li>
						<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
							<?php get_template_part('dashboard/partials/job_summary'); ?>
						</article> <!-- .et_pb_post -->
					</li>
				<?php endwhile; ?>
			</ul>
		<?php
					if ( function_exists( 'wp_pagenavi' ) )
						wp_pagenavi();
					else
						get_template_part( 'includes/navigation', 'index' );
				else :
					get_template_part( 'dashboard/partials/no-results-jobs' );
				endif; ?>
			</div> <!-- #job-area -->
			<!--The Testimonials-->
			<?php if (get_field('intro_text', $taxonomyTerm)) : 
				$testimonial_id = get_field('testimonials_slider', $taxonomyTerm); ?>
			<section class="state-testimonials">        
				<div class="testimonial-slider">
					<?php echo do_shortcode("[sp_testimonials_slider category='$testimonial_id']"); ?>
				</div>
			</section>
			<?php endif; ?>
			<!--END Testimonials-->
			<!--How to Become a Travel Nurse Section-->
			<?php if( have_rows('three_steps', $taxonomyTerm) ): 
				while ( have_rows('three_steps', $taxonomyTerm) ) : the_row();?>
			<section class="become-travel-nurse">
				<div class="et_pd_row">
					<h2>How To Become a Travel Nurse in <?php single_term_title(); ?> With Trusted Nurse Staffing in 3 Simple Steps</h2>
					<div class="steps">
						<div class="become-step step1">
							<div class="blurb-image">
								<span class="et-waypoint et_pb_animation_top et-pb-icon et-animated">l</span>
							</div>
							<div class="blurb-content">
							<?php echo get_sub_field('step_one', $taxonomyTerm); ?>
							</div>
						</div>
						<div class="become-step step2">
							<div class="blurb-image">
								<span class="et-waypoint et_pb_animation_top et-pb-icon et-animated">U</span>
							</div>
							<div class="blurb-content">
							<?php echo get_sub_field('step_two', $taxonomyTerm); ?>
							</div>
						</div>
						<div class="become-step step3">
							<div class="blurb-image">
								<span class="et-waypoint et_pb_animation_top et-pb-icon et-animated"></span>
							</div>
							<div class="blurb-content">
							<?php echo get_sub_field('step_three', $taxonomyTerm); ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php endwhile;
			endif; ?><!--END of How to Become a Travel Nurse Section-->
			<!--The Why Section-->
			<?php if (get_field('why_section_text', $taxonomyTerm)) : ?>
				<section class="state-why-section">
					<div class="et_pb_row">
						<h2>Why Should You Consider Becoming a Travel Nurse in <?php single_term_title(); ?>?</h2>
							<div class="why-content"><?php echo get_field('why_section_text', $taxonomyTerm); ?></div>
					</div>
				</section>
			<?php endif; ?>
			<!--END of the Why Section-->
			<!--The High Demand Section-->
			<?php if (get_field('high-demand_specialities', $taxonomyTerm)) : ?>
				<section class="state-high-demand-section">
					<div class="et_pb_row">
						<h2>What Are The High-Demand Travel Nursing Specialities in <?php single_term_title(); ?> Right Now?</h2>
						<div class="state-specialties">
						<?php while( have_rows('high-demand_specialities', $taxonomyTerm) ): the_row(); ?>
							<div class="high-demand-spec"><?php echo get_sub_field('state_specialty', $taxonomyTerm); ?></div>
						<?php endwhile; ?>
							</div>
					</div>
				</section>
			<?php endif; ?>
			<!--END of the High Demand Section-->
			
			<!--Start of the FAQs-->
			<?php if(have_rows('job_state_faqs', $taxonomyTerm) ): ?>
			<section id="<?php single_term_title(); ?>-faq" class="state-faqs">
			<div class="et_pb_row">
			<h2>FAQs About <?php single_term_title(); ?> Travel Nursing</h2>
					<?php while( have_rows('job_state_faqs', $taxonomyTerm) ): the_row();
				$index = get_row_index($taxonomyTerm);
				$question = get_sub_field('faq_question', $taxonomyTerm);
				$answer = get_sub_field('faq_answer', $taxonomyTerm);
				?>
					<?php echo do_shortcode("[et_pb_toggle admin_label='Toggle' id='state-faq-$index' title='$question' open='off' use_border_color='off' border_color='#ffffff' border_style='solid'] $answer [/et_pb_toggle]") ?>
					<?php endwhile; ?>
			</div>
			</section>
			<?php endif; ?><!--END of FAQs-->
			
			<!--Start of the Map Section-->
			<div class="state-search-map">
				<div class="map-instructions">
					See jobs by state. Choose which state to explore. 
				</div>
				<?php echo do_shortcode('[display-map id="1855"]'); ?>
			</div><!--END of Map Area-->
			<div class="state-directory-link">
					<a class="et_pb_button et_pb_more_button et_pb_button_one" href="/state/">BACK TO STATE DIRECTORY</a>
			</div>
			<section class="create-account-button">
				<a href="/your-account/"><button>Create a Free Profile</button></a>
				<div>To See Exclusive Travel Nursing Jobs in <?php single_term_title(); ?></div>
			</section>
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>