<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION

include get_stylesheet_directory().'/dashboard/dashboard.php';

add_action( 'update_jobs', 'update_jobs' );
function update_jobs()
{
    $log = ["Starting... " . time()];

    // $contents = file_get_contents('https://trustednursestaffing.secure.force.com/ApplyToJob/TNS_ExternalJobFeeds');
    $options = array(
        CURLOPT_RETURNTRANSFER => true,   // return web page
        CURLOPT_HEADER         => false,  // don't return headers
        CURLOPT_FOLLOWLOCATION => true,   // follow redirects
        CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
        CURLOPT_ENCODING       => "",     // handle compressed
        CURLOPT_USERAGENT      => "test", // name of client
        CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
        CURLOPT_TIMEOUT        => 120,    // time-out on response
    );

    $ch = curl_init('https://trustednursestaffing.secure.force.com/ApplyToJob/TNS_ExternalJobFeeds');
    curl_setopt_array($ch, $options);

    $contents  = curl_exec($ch);

    curl_close($ch);

	$data = simplexml_load_string($contents);

    $state_abbr_map = [
        "AL" => "Alabama",
        "AK" => "Alaska",
        "AZ" => "Arizona",
        "AR" => "Arkansas",
        "CA" => "California",
        "CO" => "Colorado",
        "CT" => "Connecticut",
        "DE" => "Delaware",
        "DC" => "District of Colombia",
        "FL" => "Florida",
        "GA" => "Georgia",
        "HI" => "Hawaii",
        "ID" => "Idaho",
        "IL" => "Illinois",
        "IN" => "Indiana",
        "IA" => "Iowa",
        "KS" => "Kansas",
        "KY" => "Kentucky",
        "LA" => "Louisiana",
        "ME" => "Maine",
        "MD" => "Maryland",
        "MA" => "Massachusetts",
        "MI" => "Michigan",
        "MN" => "Minnesota",
        "MS" => "Mississippi",
        "MO" => "Missouri",
        "MT" => "Montana",
        "NE" => "Nebraska",
        "NV" => "Nevada",
        "NH" => "New Hampshire",
        "NJ" => "New Jersey",
        "NM" => "New Mexico",
        "NY" => "New York",
        "NC" => "North Carolina",
        "ND" => "North Dakota",
        "OH" => "Ohio",
        "OK" => "Oklahoma",
        "OR" => "Oregon",
        "PA" => "Pennsylvania",
        "PR" => "Puerto Rico",
        "RI" => "Rhode Island",
        "SC" => "South Carolina",
        "SD" => "South Dakota",
        "TN" => "Tennessee",
        "TX" => "Texas",
        "UT" => "Utah",
        "VT" => "Vermont",
        "VA" => "Virginia",
        "WA" => "Washington",
        "WV" => "West Virginia",
        "WI" => "Wisconsin",
        "WY" => "Wyoming",
    ];

    foreach ($data->job as $job) {
        $start_date = (string)$job->Start_Date;
        if (strtotime($start_date) < strtotime('yesterday')) {
            continue;
        }

        $log[] = "processing " . (string)$job->Job_ID;

        $job_state = (string)$job->State;
        if (strlen($job_state) == 2) {
            $job_state = $state_abbr_map[$job_state];
        }

        $attrs = [
            'job_id' => (string)$job->Job_ID,
            'company' => (string)$job->Company,
            'type' => (string)$job->RecordType,
            'title' => (string)$job->Job_Title,
            'profession' => (string)$job->Profession,
            'specialty' => (string)$job->Specialty,
            'description' => (string)$job->Description,
			'shift' => (string)$job->Shift,
            'city' => (string)$job->City,
            'state' => $job_state,
            'zip' => (string)$job->Postal_Code,
            'hot' => (string)$job->Hot_Job_Indicator,
            'duration' => (string)$job->Contract_Length,
            'start_date' => (string)$job->Start_Date,
			'apply_url' => (string)$job->Apply_URL,
			'company' => (string)$job->Company,
			'regional_area' => (string)$job->Regional_Area,
        ];

		try {
			$posts = get_posts(array(
				'numberposts' => -1,
				'post_type' => 'job',
				'meta_query' => array(
					array(
						'key'   => 'job_id',
						'value' => $attrs['job_id'],
					),
				),
			));
		} catch (Exception $e) {
            $log[] = "  Failed getting posts: {$e->getMessage()}";
			continue;
		}

        if (!$posts) {
            $new_job_id = wp_insert_post([
				'post_title' => $attrs['title'],
				'post_type' => 'job',
				'post_status' => 'publish',
                'posts_per_page' => 8,
				'post_author'   => 1,
            ]);

            $log[] = "  Adding new job";

            foreach ($attrs as $k => $v) {
                if (in_array($k, [
                    'specialty',
                    'profession',
                    'state',
                ])) {
                    wp_set_object_terms( $new_job_id, $v, $k );
                } else {
                    add_post_meta($new_job_id, $k, $v);
                }
            }
		} else {
            $log[] = "  Posts found. Skipping: " . json_encode($posts);
        }
    }

    $log = implode("<br/>\n", $log);

    if (isset($_REQUEST['alextest'])) {
        echo $log;
    }

    file_put_contents(get_home_path() . "/jobs-cron/" . time(), $log);
    return $log;
}

if (isset($_REQUEST['alextest'])) {
    update_jobs();
    exit;
}

add_action('set_current_user', 'cc_hide_admin_bar');
function cc_hide_admin_bar() {
  if (!current_user_can('edit_posts')) {
    show_admin_bar(false);
  }
}

add_action('wp_login', function () {
    if ($redirect = $_COOKIE['ts_redirect']) {
        unset($_COOKIE['ts_redirect']);
        setcookie("ts_redirect", "", time() - 3600, '/');
        header("Location: {$redirect}");
        exit;
    }
}, 10, 0);

// Display current year
function year_shortcode() {
$year = date_i18n('Y');
return $year; } add_shortcode
(
'year', 'year_shortcode');

//Changing the number of jobs displayed per page

function my_wp_is_mobile() {
    static $is_mobile;

    if ( isset($is_mobile) )
        return $is_mobile;

    if ( empty($_SERVER['HTTP_USER_AGENT']) ) {
        $is_mobile = false;
    } elseif (
        strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Silk/') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Kindle') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry') !== false
        || strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false ) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') == false) {
            $is_mobile = true;
    } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false) {
        $is_mobile = false;
    } else {
        $is_mobile = false;
    }

    return $is_mobile;
}

//Dynamic User Profile Greeting
function welcome_message_function( $atts ) {
	global $current_user, $user_login;
      	get_currentuserinfo();
	add_filter('widget_text', 'do_shortcode');
	if ($user_login)
		return 'Welcome ' . $current_user->first_name . '!';
}
add_shortcode( 'profile-greeting', 'welcome_message_function' );

function create_hubspot_user($data)
{
    $payload = [
        // Initial values
        [
            "property" => "web_profile",
            "value" => "Yes",
        ],
    ];
    foreach ($data as $field => $value) {
        if (!$field || !$value) {
            continue;
        }

        $payload[] = [
            "property" => $field,
            "value" => $value,
        ];
    }

    $api_key = '9513affc-fa69-43a6-b2a7-508d6b98704f';

    $ch = curl_init("https://api.hubapi.com/contacts/v1/contact/?hapikey=$api_key");
    # Setup request to send json via POST.
    $payload = json_encode(["properties" => $payload]);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    # Return response instead of printing.
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    # Send request.
    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    // if (isset($_GET['alex-test'])) {
        // echo "<br/><br/><br/><br/><br/>";
        // echo json_encode($payload) . "<br/><br/>";
        // print_r($result);
        // print_r($httpcode);
        // exit;
    // }


    // $options = array(
    // 'http' => array(
    //     'method'  => 'POST',
    //     'content' => json_encode( [
    //         "properties" => $payload,
    //     ] ),
    //     'header'=>  "Content-Type: application/json\r\n" .
    //                 "Accept: application/json\r\n"
    //     )
    // );

    // $context  = stream_context_create( $options );
    // $result = file_get_contents( "https://api.hubapi.com/contacts/v1/contact/?hapikey=$api_key", false, $context );
    $response = json_decode( $result, true );
    // print "<br/><br/><br/><br/><br/><br/><br/><br/>" . $result;
    // print_r($payload);exit;
    return $response;
}

function update_hubspot_user($data, $hubspot_contact_id)
{
    $payload = [];
    foreach ($data as $field => $value) {
        if (!$value) {
            continue;
        }

        $payload[] = [
            "property" => $field,
            "value" => $value,
        ];
    }

    $api_key = '9513affc-fa69-43a6-b2a7-508d6b98704f';
    $options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode( [
            "properties" => $payload,
        ] ),
        'header'=>  "Content-Type: application/json\r\n" .
                    "Accept: application/json\r\n"
        )
    );

    // print "<br/><br/><br/><br/><br/><br/><br/><br/>";
    // echo json_encode($payload);
    // echo gettype($data['preferred_profession']));exit;
    $context  = stream_context_create( $options );
    $result = file_get_contents( "https://api.hubapi.com/contacts/v1/contact/vid/$hubspot_contact_id/profile?hapikey=$api_key", false, $context );
    $response = json_decode( $result, true );
    // print "<br/><br/><br/><br/><br/><br/><br/><br/>" . $result;exit;
    return $response;
}

function post_save_integration($post_id)
{
    if (!preg_match('#user_(\d+)$#', $post_id, $matches)) {
        return;
    }

    // If the user is being created, set 'dream job' and 'state' to whatever their
    // current profession / license state is.
    if (!get_field('location_of_interest')) {
        // State to taxonomy map
        $state_abbr_map = [
            "AL" => "Alabama",
            "AK" => "Alaska",
            "AZ" => "Arizona",
            "AR" => "Arkansas",
            "CA" => "California",
            "CO" => "Colorado",
            "CT" => "Connecticut",
            "DE" => "Delaware",
            "DC" => "District of Colombia",
            "FL" => "Florida",
            "GA" => "Georgia",
            "HI" => "Hawaii",
            "ID" => "Idaho",
            "IL" => "Illinois",
            "IN" => "Indiana",
            "IA" => "Iowa",
            "KS" => "Kansas",
            "KY" => "Kentucky",
            "LA" => "Louisiana",
            "ME" => "Maine",
            "MD" => "Maryland",
            "MA" => "Massachusetts",
            "MI" => "Michigan",
            "MN" => "Minnesota",
            "MS" => "Mississippi",
            "MO" => "Missouri",
            "MT" => "Montana",
            "NE" => "Nebraska",
            "NV" => "Nevada",
            "NH" => "New Hampshire",
            "NJ" => "New Jersey",
            "NM" => "New Mexico",
            "NY" => "New York",
            "NC" => "North Carolina",
            "ND" => "North Dakota",
            "OH" => "Ohio",
            "OK" => "Oklahoma",
            "OR" => "Oregon",
            "PA" => "Pennsylvania",
            "PR" => "Puerto Rico",
            "RI" => "Rhode Island",
            "SC" => "South Carolina",
            "SD" => "South Dakota",
            "TN" => "Tennessee",
            "TX" => "Texas",
            "UT" => "Utah",
            "VT" => "Vermont",
            "VA" => "Virginia",
            "WA" => "Washington",
            "WV" => "West Virginia",
            "WI" => "Wisconsin",
            "WY" => "Wyoming",
        ];

        $state_taxonomies = get_terms(array(
            'taxonomy' => 'state',
            'hide_empty' => false,
        ));

        $license_info = get_field('license_information', $post_id);
        $licensed_states = [];
        foreach ($license_info as $li) {
            $license_state_full = $state_abbr_map[$li['license_state']];
            foreach ($state_taxonomies as $st) {
                if (strtolower($license_state_full) === strtolower($st->name)) {
                    $licensed_states[] = $st->term_taxonomy_id;
                }
            }
        }

        update_field('location_of_interest', $licensed_states, $post_id);
    }

    // echo "<br/><br/><br/><br/><br/><br/><br/><br/>value: ";
    // print_r(get_field('personal_information', 'user_15'));exit;

    $user_id = $matches[1];

    $data = [];
    $groups = [
        'personal_information' => [
            'repeater' => false,
            'fields' => [
                "first_name" => [
                    "hs_field" => "firstname",
                ],
                "last_name" => [
                    "hs_field" => "lastname",
                ],
                "email" => [
                    "hs_field" => "email",
                ],
                "phone" => [
                    "hs_field" => "phone",
                ],
                "address_1" => [
                    "hs_field" => "address",
                ],
                "address_2" => [
                    "hs_field" => "",
                ],
                "city" => [
                    "hs_field" => "city",
                ],
                "state" => [
                    "hs_field" => "state",
                ],
                "zip_code" => [
                    "hs_field" => "zip",
                ],
                "date_of_birth" => [
                    "hs_field" => "date_of_birth",
                ],
                "work_status" => [
                    "hs_field" => "work_status",
                ],
                "other_work_status" => [
                    "hs_field" => "work_status",
                ],
            ],
        ],
        'profession' => [
            'repeater' => false,
            'fields' => [
                "profession-type" => [
                    "hs_field" => "profession_type",
                ],
                "nursing_profession" => [
                    "hs_field" => "preferred_profession",
                ],
                "allied_profession" => [
                    "hs_field" => "preferred_profession",
                ],
                "advanced_practice_profession" => [
                    "hs_field" => "preferred_profession",
                ],
                "non_clinical_practice" => [
                    "hs_field" => "preferred_profession",
                ],
                "years_of_experience" => [
                    "hs_field" => "years_of_experience",
                ],
                "primary_specialty" => [
                    "hs_field" => "primary_speciality",
                ],
                "secondary_specialty" => [
                    "hs_field" => "secondary_specialty",
                ],
                "preferred_state_date" => [
                    "hs_field" => "preferred_start_date",
                ],
                "asap" => [
                    "hs_field" => "start_asap",
                ],
            ],
        ],
        'license_information' => [
            'repeater' => true,
            'fields' => [
                "license_type" => [
                    "hs_field" => "license_information",
                    "type" => "multi_line",
                ],
                "license_state" => [
                    "hs_field" => "license_information",
                    "type" => "multi_line",
                ],
                "license_number" => [
                    "hs_field" => "license_information",
                    "type" => "multi_line",
                ],
                "expiration_date" => [
                    "hs_field" => "license_information",
                    "type" => "multi_line",
                ],
                "compact_license" => [
                    "hs_field" => "license_information",
                    "type" => "multi_line",
                ],
            ],
        ],
        'work_history' => [
            'repeater' => true,
            'nested' => 'work_history',
            'fields' => [
                "facility" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "type" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "position" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "teaching_facility" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "location" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "state_date" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "end_date" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "current_position" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "specialty" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "shift" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "charge_experience" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "nurse_to_patient_ratio" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "beds_in_unit" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "#_beds_in_facility" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "reason_for_leaving" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "reference_name" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "reference_phone" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
                "ok_to_contact" => [
                    "hs_field" => "work_history",
                    "type" => "multi_line",
                ],
            ],
        ],
        'education' => [
            'repeater' => true,
            'nested' => 'degrees',
            'fields' => [
                "degree" => [
                    "hs_field" => "education_information",
                    "type" => "multi_line",
                ],
                "school" => [
                    "hs_field" => "education_information",
                    "type" => "multi_line",
                ],
                "location" => [
                    "hs_field" => "education_information",
                    "type" => "multi_line",
                ],
                "started" => [
                    "hs_field" => "education_information",
                    "type" => "multi_line",
                ],
                "graduated" => [
                    "hs_field" => "education_information",
                    "type" => "multi_line",
                ],
                "currently_attending" => [
                    "hs_field" => "education_information",
                    "type" => "multi_line",
                ],
            ],
        ],
        'references' => [
            'repeater' => true,
            'fields' => [
                "facilityhospital" => [
                    "hs_field" => "references",
                    "type" => "multi_line",
                ],
                "name" => [
                    "hs_field" => "references",
                    "type" => "multi_line",
                ],
                "title" => [
                    "hs_field" => "references",
                    "type" => "multi_line",
                ],
                "email" => [
                    "hs_field" => "references",
                    "type" => "multi_line",
                ],
                "phone" => [
                    "hs_field" => "references",
                    "type" => "multi_line",
                ],
            ],
        ],
        'credentials' => [
            'repeater' => true,
            'fields' => [
                "certification" => [
                    "hs_field" => "credentials",
                    "type" => "multi_line",
                ],
                "other_certification" => [
                    "hs_field" => "credentials",
                    "type" => "multi_line",
                ],
                "issue_date" => [
                    "hs_field" => "credentials",
                    "type" => "multi_line",
                ],
                "expiration_date" => [
                    "hs_field" => "credentials",
                    "type" => "multi_line",
                ],
            ],
        ],
        'fit_test' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'fit_test',
            'fields' => [
                "test_date" => [
                    "hs_field" => "fit_test",
                    "type" => "multi_line",
                ],
                "expiration_date" => [
                    "hs_field" => "fit_test",
                    "type" => "multi_line",
                ],
            ],
        ],
        'flu_shot' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'flu_shot',
            'fields' => [
                "test_date" => [
                    "hs_field" => "flu_shot",
                    "type" => "multi_line",
                ],
                "expiration_date" => [
                    "hs_field" => "flu_shot",
                    "type" => "multi_line",
                ],
            ],
        ],
        'hep_b' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'hep_b',
            'fields' => [
                "date_1" => [
                    "hs_field" => "hep_b_vaccines",
                    "type" => "multi_line",
                ],
                "date_2" => [
                    "hs_field" => "hep_b_vaccines",
                    "type" => "multi_line",
                ],
                "date_3" => [
                    "hs_field" => "hep_b_vaccines",
                    "type" => "multi_line",
                ],
            ],
        ],
        'heb_b_titre' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'hep_b_titre',
            'fields' => [
                "titre_date" => [
                    "hs_field" => "heb_b_titre",
                    "type" => "multi_line",
                ],
                "result_-_positive" => [
                    "hs_field" => "heb_b_titre",
                    "type" => "multi_line",
                ],
                "result_-_negative" => [
                    "hs_field" => "heb_b_titre",
                    "type" => "multi_line",
                ],
            ],
        ],
        'mmr' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'mmr',
            'fields' => [
                "date_1" => [
                    "hs_field" => "mmr_vaccines",
                    "type" => "multi_line",
                ],
                "date_2" => [
                    "hs_field" => "mmr_vaccines",
                    "type" => "multi_line",
                ],
            ],
        ],
        'mmr_titre' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'mmr_titre',
            'fields' => [
                "titre_date" => [
                    "hs_field" => "mmr_titre",
                    "type" => "multi_line",
                ],
                "result_-_positive" => [
                    "hs_field" => "mmr_titre",
                    "type" => "multi_line",
                ],
                "result_-_negative" => [
                    "hs_field" => "mmr_titre",
                    "type" => "multi_line",
                ],
            ],
        ],
        'physical' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'physical',
            'fields' => [
                "test_date" => [
                    "hs_field" => "physical",
                    "type" => "multi_line",
                ],
                "expiration_date" => [
                    "hs_field" => "physical",
                    "type" => "multi_line",
                ],
            ],
        ],
        'ppd' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'ppd',
            'fields' => [
                "place_date" => [
                    "hs_field" => "ppd",
                    "type" => "multi_line",
                ],
                "read_date" => [
                    "hs_field" => "ppd",
                    "type" => "multi_line",
                ],
                "expiration_date" => [
                    "hs_field" => "ppd",
                    "type" => "multi_line",
                ],
            ],
        ],
        'ppd_2' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'ppd_2',
            'fields' => [
                "place_date" => [
                    "hs_field" => "n2nd_step_ppd",
                    "type" => "multi_line",
                ],
                "read_date" => [
                    "hs_field" => "n2nd_step_ppd",
                    "type" => "multi_line",
                ],
                "expiration_date" => [
                    "hs_field" => "n2nd_step_ppd",
                    "type" => "multi_line",
                ],
            ],
        ],
        'igra_test' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'igra_test',
            'fields' => [
                "test_date" => [
                    "hs_field" => "igra_test",
                    "type" => "multi_line",
                ],
                "expiration_date" => [
                    "hs_field" => "igra_test",
                    "type" => "multi_line",
                ],
            ],
        ],
        'tdap' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'igra_test',
            'fields' => [
                "date" => [
                    "hs_field" => "tdap",
                    "type" => "multi_line",
                ],
                "expiration_date" => [
                    "hs_field" => "tdap",
                    "type" => "multi_line",
                ],
            ],
        ],
        'varicella' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'varicella',
            'fields' => [
                "date_1" => [
                    "hs_field" => "varicella_vaccines",
                    "type" => "multi_line",
                ],
                "date_2" => [
                    "hs_field" => "varicella_vaccines",
                    "type" => "multi_line",
                ],
            ],
        ],
        'varicella_titre' => [
            'repeater' => false,
            'group_field_name' => 'medical_history',
            'nested' => 'varicella_titre',
            'fields' => [
                "titre_date" => [
                    "hs_field" => "varicella_titre",
                    "type" => "multi_line",
                ],
                "result_-_positive" => [
                    "hs_field" => "varicella_titre",
                    "type" => "multi_line",
                ],
                "result_-_negative" => [
                    "hs_field" => "varicella_titre",
                    "type" => "multi_line",
                ],
            ],
        ],
    ];

    // echo "<br/><br/><br/><br/><br/>";
    // echo json_encode(get_field('work_history', $post_id));
    // exit;

    foreach ($groups as $group => $group_config) {
        // Handle grouped data
        $group = $group_config['group_field_name'] ?? $group;
        $grouped_fields = get_field($group, $post_id);
        if (isset(($group_config['nested']))) {
            $grouped_fields = $grouped_fields[$group_config['nested']];
        }

        if ($group_config['repeater'] === false) {
            $grouped_fields = [$grouped_fields];
        }

        $fields = $group_config['fields'];

        foreach ($grouped_fields as $group_index => $group_fields) {
            $extra_line_multi_line = false;

            foreach ($group_fields as $key => $value) {
                $config = $fields[$key] ?? null;
                if (!$config) {
                    continue;
                }

                // echo "$key: $value<br/>";

                $hs_field = $config['hs_field'];
                $field_type = $config['type'] ?? 'single_line';

                if (!isset($data[$hs_field])) {
                    switch ($field_type) {
                        case 'multi_line':
                            $data[$hs_field] = "$key: $value";
                            break;
                        default:
                            $data[$hs_field] = $value;
                            break;
                    }
                } else {
                    switch ($field_type) {
                        case 'single_line':
                            $data[$hs_field] = $data[$hs_field] . ", " . $value;
                            break;
                        case 'multi_line':
                            if ($group_index > 0 && $extra_line_multi_line === false) {
                                $data[$hs_field] = $data[$hs_field] . "\n";
                                $extra_line_multi_line = true;
                            }
                            $data[$hs_field] = $data[$hs_field] . "\n$key: " . $value;
                            break;
                    }
                }
            }
        }
    }

    // print_r($data);
    // exit;

    $hubspot_id = get_user_meta($user_id, 'hubspot_contact_id', true);

    if (!$hubspot_id) {
        $new_user = create_hubspot_user($data);
        update_user_meta($user_id, 'hubspot_contact_id', $new_user['vid']);
    } else {
        update_hubspot_user($data, $hubspot_id);
    }


}
add_action('acf/save_post', 'post_save_integration', 15, 1);

function wpdocs_set_html_mail_content_type() {
    return 'text/html';
}
add_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );
