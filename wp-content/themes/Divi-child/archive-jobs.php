<?php

 get_header();

wp_enqueue_script('dash-scripts');
wp_enqueue_style('dash-styles');
 ?>
<div id="job-search-hero">
	<h1>Find Your Dream Job</h1>
</div>
<div id="main-content">
	<div class="container search">
		<div id="content-area" class="clearfix">
			<div class="state-search-map">
				<div class="map-instructions">
					See jobs by state. Choose which state to explore. 
				</div>
				<?php echo do_shortcode('[display-map id="1855"]'); ?>
			</div>
			<div class="search-form">
					<?php echo do_shortcode('[searchandfilter fields="state,profession,specialty" post_type="job" submit_label="Search Jobs"]'); ?>
				</div>
			<div id="job-area">
				<div class="job-icon-key">
					<!--<?php if (is_user_logged_in()) : ?>
					<div class="job-icon">
						<i class="far fa-hospital"></i> Facility
					</div>
					<?php endif; ?>
					<div class="job-icon">
						<i class="fas fa-medkit"></i> Profession
					</div>
					<div class="job-icon">
						<i class="fas fa-stethoscope"></i> Specialties
					</div>-->
					<div class="job-icon">
						<i class="fas fa-map-marker-alt"></i> Location
					</div>
					<div class="job-icon">
						<i class="far fa-calendar-check"></i> Start Date
					</div>
					<div class="job-icon">
						<i class="far fa-clock"></i> Shift
					</div>
					<div class="job-icon">
						<i class="fas fa-hourglass-half"></i> Contract Length
					</div>
					<?php if (is_user_logged_in()) : ?>
					<div class="job-icon">
						<i class="fas fa-dollar-sign"></i> Pay Range
					</div>	
					<?php endif; ?>
				</div>
		<?php if ( have_posts() ) : ?>
			<ul class="grid-search">
				<?php while ( have_posts() ) : the_post();
					$post_format = et_pb_post_format(); ?>
					<li>
						<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
							<?php get_template_part('dashboard/partials/job_summary'); ?>
						</article> <!-- .et_pb_post -->
					</li>
				<?php endwhile; ?>
			</ul>
		<?php
					if ( function_exists( 'wp_pagenavi' ) )
						wp_pagenavi();
					else
						get_template_part( 'includes/navigation', 'index' );
				else :
					get_template_part( 'dashboard/partials/no-results-jobs' );
				endif;
			?>
			</div> <!-- #job-area -->

		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->

<?php get_footer(); ?>